package com.example.removebackgrounddemov2


import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import android.util.Size
import androidx.core.graphics.scale
import androidx.core.graphics.set
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.CompatibilityList
import org.tensorflow.lite.gpu.GpuDelegate
import java.io.FileInputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.channels.FileChannel


class BgRemove(private val context: Context) {

    companion object {
        private const val MODEL_PATH = "u2net_fp16.tflite"
        private const val NUM_THREADS = 4
        private val IMAGE_MEAN = floatArrayOf(0.485f, 0.456f, 0.406f)
        private val IMAGE_STD = floatArrayOf(0.229f, 0.224f, 0.225f)
        private var INPUT_IMAGE_WIDTH: Int = 0
        private var INPUT_IMAGE_HEIGHT: Int = 0
        private const val INPUT_CHANNELS: Int = 3
        private var OUTPUT_IMAGE_WIDTH: Int = 0
        private var OUTPUT_IMAGE_HEIGHT: Int = 0
        private const val INPUT_TENSOR_INDEX = 0
        private const val OUTPUT_TENSOR_INDEX = 0
    }

    private lateinit var model: Interpreter
    private lateinit var inBuffer: ByteBuffer

    @Throws(IOException::class)
    private fun loadModel() {
        val fileDescriptor = context.assets.openFd(MODEL_PATH)
        val fileChannel = FileInputStream(fileDescriptor.fileDescriptor).channel
        val modelFile = fileChannel.map(
            FileChannel.MapMode.READ_ONLY,
            fileDescriptor.startOffset,
            fileDescriptor.declaredLength
        )


        val compatList = CompatibilityList()
        var options = Interpreter.Options().apply {
            if (compatList.isDelegateSupportedOnThisDevice) {
                // if the device has a supported GPU, add the GPU delegate
                Log.d("DEMO", "GPU available")
                val delegateOptions = compatList.bestOptionsForThisDevice
                this.addDelegate(GpuDelegate(delegateOptions))
            } else {
                // if the GPU is not supported, run on 4 threads
                this.setNumThreads(NUM_THREADS)
                Log.d("DEMO", "GPU is not supported. Use $NUM_THREADS thread(s)")
            }
        }

        try {
            model = Interpreter(modelFile, options)
        } catch (e: IllegalArgumentException) {
            Log.d("DEMO", "Cannot use GPU. Switch to CPU version")
            options = Interpreter.Options()
            options.setNumThreads(NUM_THREADS)
            options.setUseXNNPACK(true)
            model = Interpreter(modelFile, options)
        }
        val inputShape =
            model.getInputTensor(INPUT_TENSOR_INDEX).shape() // {1, 3, height, width}
        assert(inputShape[1] == INPUT_CHANNELS) {
            "Invalid model file"
        }
        INPUT_IMAGE_HEIGHT = inputShape[2]
        INPUT_IMAGE_WIDTH = inputShape[3]

        val outputShape =
            model.getOutputTensor(OUTPUT_TENSOR_INDEX).shape() // {1, 1, height, width}
        OUTPUT_IMAGE_HEIGHT = outputShape[2]
        OUTPUT_IMAGE_WIDTH = outputShape[3]
        inBuffer =
            ByteBuffer.allocateDirect(INPUT_CHANNELS * INPUT_IMAGE_HEIGHT * INPUT_IMAGE_WIDTH * Float.SIZE_BYTES)
        inBuffer.order(ByteOrder.nativeOrder())
        Log.d(
            "TFLiteBgrm",
            "Load model success. Input shape = ${inputShape.contentToString()}. Output Shape: ${outputShape.contentToString()}"
        )
    }

    fun release() {
        model.close()
    }

    init {
        loadModel()
    }

    private fun resizeImage(bitmap: Bitmap, targetSize: Size): Bitmap {
        return bitmap.scale(targetSize.width, targetSize.height)
    }

    fun removeBackground(inputBitmap: Bitmap): Bitmap {
        val resized = resizeImage(inputBitmap, Size(INPUT_IMAGE_WIDTH, INPUT_IMAGE_HEIGHT))
        val floatArray: FloatArray = imageToArray(resized)

        val maxValue = floatArray.maxOrNull()!!
        val normalized = floatArray.mapIndexed { index, value ->
            val c = index / (INPUT_IMAGE_HEIGHT * INPUT_IMAGE_WIDTH)
            (value / maxValue - IMAGE_MEAN[c]) / IMAGE_STD[c]
        }.toFloatArray()

        inBuffer.clear()
        inBuffer.asFloatBuffer().put(normalized)
        inBuffer.rewind()

        val outBuffer = FloatBuffer.allocate(OUTPUT_IMAGE_HEIGHT * OUTPUT_IMAGE_WIDTH)
        model.run(inBuffer, outBuffer)
        outBuffer.rewind()

        val outFloatArray = outBuffer.array()
        val outputMask: Bitmap = outputArrayToMask(outFloatArray)
        val resizedMask: Bitmap =
            outputMask.scale(inputBitmap.width, inputBitmap.height, filter = true)
        return applyMask(inputBitmap, resizedMask)
    }

    private fun imageToArray(inputBitmap: Bitmap): FloatArray {
        val intArray = IntArray(inputBitmap.width * inputBitmap.height)
        inputBitmap.getPixels(
            intArray,
            0,
            inputBitmap.width,
            0,
            0,
            inputBitmap.width,
            inputBitmap.height
        )
        val floatArray = FloatArray(INPUT_CHANNELS * INPUT_IMAGE_HEIGHT * INPUT_IMAGE_WIDTH)
        intArray.forEachIndexed { index, colorValue ->
            val y = index / inputBitmap.width
            val x = index % inputBitmap.width
            val color = Color.valueOf(colorValue)
            floatArray[0 * inputBitmap.height * inputBitmap.width + y * inputBitmap.width + x] =
                color.red()
            floatArray[1 * inputBitmap.height * inputBitmap.width + y * inputBitmap.width + x] =
                color.green()
            floatArray[2 * inputBitmap.height * inputBitmap.width + y * inputBitmap.width + x] =
                color.blue()
        }
        return floatArray
    }

    private fun outputArrayToMask(outputArray: FloatArray): Bitmap {
        val minFgValue = outputArray.minOrNull()!!
        val maxFgValue = outputArray.maxOrNull()!!
        val deltaMaxMin = maxFgValue - minFgValue
        val normalizedArray = outputArray.map { (it - minFgValue) / deltaMaxMin }
        val maskArray: IntArray = normalizedArray.map { Color.argb(it, it, it, it) }.toIntArray()
        val outputMask =
            Bitmap.createBitmap(OUTPUT_IMAGE_WIDTH, OUTPUT_IMAGE_HEIGHT, Bitmap.Config.ARGB_8888)
        outputMask.setPixels(
            maskArray,
            0,
            outputMask.width,
            0,
            0,
            outputMask.width,
            outputMask.height
        )
        return outputMask
    }

    private fun applyMask(inputBitmap: Bitmap, mask: Bitmap): Bitmap {
        val inputValues = IntArray(inputBitmap.width * inputBitmap.height)
        inputBitmap.getPixels(
            inputValues,
            0,
            inputBitmap.width,
            0,
            0,
            inputBitmap.width,
            inputBitmap.height
        )
        val maskValues = IntArray(mask.width * mask.height)
        mask.getPixels(maskValues, 0, mask.width, 0, 0, mask.width, mask.height)

        val outputValues = inputValues.zip(maskValues) { inputVal, maskVal ->
            val color = Color.valueOf(inputVal)
            val alpha = Color.valueOf(maskVal).alpha()
            return@zip Color.rgb(color.red() * alpha, color.green() * alpha, color.blue() * alpha)
        }.toIntArray()

        val outputBitmap = inputBitmap.copy(Bitmap.Config.ARGB_8888, true)
        outputBitmap.setPixels(
            outputValues,
            0,
            inputBitmap.width,
            0,
            0,
            inputBitmap.width,
            inputBitmap.height
        )
        return outputBitmap
    }

}