package com.example.removebackgrounddemov2

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ProgressBar
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.example.removebackgrounddemov2.databinding.ActivityMainBinding
import java.io.IOException
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var bgrm: BgRemove
    private var selectedBitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            bgrm = BgRemove(this)
        } catch (e: IOException) {
            Toast.makeText(this, "ERROR: Unable to create the module instance", Toast.LENGTH_LONG)
                .show()
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val startForResult =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == Activity.RESULT_OK && it.data?.data != null) {
                    val imageURI = Uri.parse(it.data!!.data!!.toString())
                    Log.d("DEMO", "Image URI:$imageURI")
                    val source = ImageDecoder.createSource(contentResolver, imageURI)
                    selectedBitmap = ImageDecoder.decodeBitmap(source) { decoder, _, _ ->
                        decoder.isMutableRequired = true
                    }

                    binding.imageView.setImageBitmap(selectedBitmap)
                }
            }

        binding.galleryButton.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            if (intent.resolveActivity(packageManager) != null) {
                startForResult.launch(intent)
            } else {
                Log.e("DEMO", "Unable to start intent ACTION_GET_CONTENT")
            }
        }

        binding.playButton.setOnClickListener {
            selectedBitmap?.let {
                binding.progressBar.visibility = ProgressBar.VISIBLE
                thread {
                    val outputBitmap = bgrm.removeBackground(it)
                    runOnUiThread {
                        binding.imageView.setImageBitmap(outputBitmap)
                        binding.progressBar.visibility = ProgressBar.GONE
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        bgrm.release()
        super.onDestroy()
    }

}