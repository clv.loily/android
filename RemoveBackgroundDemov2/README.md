# Background Removal v2

This version uses the original U2Net pretrained model on the saliency object detection.
The model is then converted to TFLite with floating point 16 format.

**Model Information**:
Input shape: `[1, 3, 320, 320]`
Output shape: `[1, 1, 320, 320]`

# Requirement
- Tested on Realme 6 Pro phone
- Android 11 or above might work

# How to build
- Clone this repository
- Build APK using the Android Studio

# Demo
![](screenshots/Screenshot_20220223_154738.png)
![](screenshots/Screenshot_20220223_154832.png)
![](screenshots/Screenshot_20220223_154839.png)
