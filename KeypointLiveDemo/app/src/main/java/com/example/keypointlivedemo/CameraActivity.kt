package com.example.keypointlivedemo

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.*
import android.media.Image
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.view.TextureView
import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.example.keypointlivedemo.databinding.ActivityCameraBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.common.util.concurrent.ListenableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.system.measureTimeMillis


class CameraActivity : AppCompatActivity() {
    private val REQUEST_CODE_CAMERA_PERMISSION = 200
    private val PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    private lateinit var binding: ActivityCameraBinding
    private lateinit var cameraExecutor: ExecutorService
    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var imageAnalysis: ImageAnalysis

    private lateinit var keypointAnalyzer: KeypointsAnalyzer
    private var isUsingAnalyzer: Boolean = false

    protected var mBackgroundThread: HandlerThread? = null
    protected var mBackgroundHandler: Handler? = null
    protected var mUIHandler: Handler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCameraBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mUIHandler = Handler(mainLooper);

        Log.d("DEMO", "Is GPU Available: ${KeypointsCropDetect.isGPUAvailable()}")
        keypointAnalyzer = KeypointsAnalyzer(this, KeypointsCropDetect.isGPUAvailable())
        startBackgroundThread()

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                PERMISSIONS,
                REQUEST_CODE_CAMERA_PERMISSION
            )
        }
        cameraExecutor = Executors.newSingleThreadExecutor()

        binding.controlButton.setOnClickListener {
            isUsingAnalyzer = !isUsingAnalyzer
            keypointAnalyzer.setEnable(isUsingAnalyzer)
            val button = it as FloatingActionButton
            if (isUsingAnalyzer) {
                val icon = ContextCompat.getDrawable(this, android.R.drawable.ic_media_pause)
                button.setImageDrawable(icon)
            } else {
                val icon = ContextCompat.getDrawable(this, android.R.drawable.ic_media_play)
                button.setImageDrawable(icon)
            }
        }

        binding.cameraPreview.surfaceTextureListener = object: TextureView.SurfaceTextureListener {
            override fun onSurfaceTextureAvailable(p0: SurfaceTexture, width: Int, height: Int) {
                cameraProviderFuture = ProcessCameraProvider.getInstance(this@CameraActivity)
                cameraProviderFuture.addListener({
                    val cameraProvider = cameraProviderFuture.get()
                    bindPreview(cameraProvider, width, height)
                }, ContextCompat.getMainExecutor(this@CameraActivity))
            }

            override fun onSurfaceTextureSizeChanged(p0: SurfaceTexture, p1: Int, p2: Int) {
            }

            override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean {
                return true
            }

            override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {
            }
        }
    }

    private fun startBackgroundThread() {
        mBackgroundThread = HandlerThread("ModuleActivity")
        mBackgroundThread!!.start()
        mBackgroundHandler = Handler(mBackgroundThread!!.looper)
    }

    private fun stopBackgroundThread() {
        mBackgroundThread!!.quitSafely()
        try {
            mBackgroundThread!!.join()
            mBackgroundThread = null
            mBackgroundHandler = null
        } catch (e: InterruptedException) {
            Log.e("DEMO", "Error on stopping background thread", e)
        }
    }

    private fun bindPreview(cameraProvider: ProcessCameraProvider?, width: Int, height: Int) {
        val preview: Preview = Preview.Builder()
            .build()

        val cameraSelector: CameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()

        preview.setSurfaceProvider(binding.previewView.surfaceProvider)
        imageAnalysis = ImageAnalysis.Builder()
            // enable the following line if RGBA output is needed.
            .setOutputImageFormat(ImageAnalysis.OUTPUT_IMAGE_FORMAT_RGBA_8888)
            .setTargetResolution(Size(width, height))
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .setOutputImageRotationEnabled(true)
            .build()

        imageAnalysis.setAnalyzer(
            ContextCompat.getMainExecutor(this),
            keypointAnalyzer
        )

        cameraProvider!!.bindToLifecycle(
            this as LifecycleOwner,
            cameraSelector,
            imageAnalysis,
            preview
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(
                    this,
                    "You can't use object detection example without granting CAMERA permission",
                    Toast.LENGTH_LONG
                )
                    .show()
                finish()
            }
        }
    }

    override fun onDestroy() {
        cameraExecutor.shutdown()
        stopBackgroundThread()
        keypointAnalyzer.release()
        super.onDestroy()
    }


    class KeypointsAnalyzer(
        private val activity: CameraActivity,
        useGPU: Boolean,
    ) : ImageAnalysis.Analyzer {

        private var isEnable = false
        private var totalFrame = 0L
        private var totalTime = 0L
        private val instance = try {
            KeypointsCropDetect.create(activity, useGPU)
        } catch (e: RuntimeException) {
            Toast.makeText(activity, "ERROR: ${e.message}", Toast.LENGTH_SHORT).show()
            KeypointsCropDetect.create(activity, false)
        }
        private val textPaint = Paint().apply {
            this.color = Color.RED
            this.textSize = 72.0f
        }

        fun setEnable(enable: Boolean) {
            if (enable != this.isEnable) {
                resetFpsCounter()
            }
            this.isEnable = enable
        }

        private fun resetFpsCounter() {
            this.totalFrame = 0L
            this.totalTime = 0L
        }

        @SuppressLint("UnsafeOptInUsageError")
        @WorkerThread
        override fun analyze(image: ImageProxy) {
            image.image?.let {
                val boxes: List<DetectionResult>
                val keypoints: List<List<PointF>>
                val inputBitmap: Bitmap
                val computeTime = measureTimeMillis {
                    inputBitmap = argbImageToBitmap(it)
                    if (isEnable) {
                        boxes = instance.boxDetection(inputBitmap)
                        keypoints = instance.keypointDetection(inputBitmap, boxes)
                    } else {
                        boxes = emptyList()
                        keypoints = emptyList()
                    }
                }
                totalFrame += 1
                totalTime += computeTime
                val frameTime = computeTime / 1000.0
                val fps = (totalFrame / (totalTime / 1000.0)).toInt()
                if (totalFrame >= 10) {
                    resetFpsCounter()
                }
                val logText = "Frame time: ${frameTime}s\nAvg FPS: $fps"
                activity.runOnUiThread {
                    activity.binding.cameraPreview.lockCanvas()?.let { canvas ->
                        canvas.drawBitmap(
                            inputBitmap,
                            0.0f,
                            0.0f,
                            null
                        )
                        visualizeKeypoints(canvas, keypoints)
                        if (isEnable) {
                            val x = 0.0f
                            var y = 0.0f
                            for (line in logText.split("\n")) {
                                y += textPaint.descent() - textPaint.ascent()
                                canvas.drawText(line, x, y, textPaint)
                            }
                        }
                        activity.binding.cameraPreview.unlockCanvasAndPost(canvas)
                    }
                }
            }
            image.close()
        }

        fun visualizeKeypoints(canvas: Canvas, boxKeypoints: List<List<PointF>>) {
            val p = Paint()
            p.style = Paint.Style.FILL
            p.isAntiAlias = true
            p.isFilterBitmap = true
            p.isDither = true
            p.color = Color.RED

            boxKeypoints.forEach { keypoints ->
                keypoints.forEach {
                    canvas.drawCircle(it.x, it.y, 15.0f, p)
                }
            }
        }

        private fun argbImageToBitmap(image: Image): Bitmap {
            val plane = image.planes[0]
            val rowPadding = plane.rowStride - plane.pixelStride * image.width
            val outputBitmap = Bitmap.createBitmap(
                image.width + rowPadding / plane.pixelStride,
                image.height,
                Bitmap.Config.ARGB_8888
            )
            outputBitmap.copyPixelsFromBuffer(plane.buffer)
            return outputBitmap
        }

        fun release() {
            instance.release()
        }

    }

}

