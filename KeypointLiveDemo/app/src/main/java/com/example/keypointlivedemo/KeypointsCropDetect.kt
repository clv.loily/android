package com.example.keypointlivedemo

import android.content.Context
import android.graphics.*
import android.util.Size
import org.tensorflow.lite.gpu.CompatibilityList
import kotlin.math.max
import kotlin.math.min

class KeypointsCropDetect(
    private val boxDetector: Yolov5Detector,
    private val keypointsDetector: KeypointsDetector,
) {

    companion object {
        fun create(context: Context, enableGPU: Boolean): KeypointsCropDetect {
            val boxDetector = Yolov5Detector(context, enableGPU)
            val keypointsDetector = KeypointsDetector(context, enableGPU)
            return KeypointsCropDetect(boxDetector, keypointsDetector)
        }

        fun isGPUAvailable(): Boolean {
            val compatList = CompatibilityList()
            return compatList.isDelegateSupportedOnThisDevice
        }
    }

    fun getSquareRect(inputRect: RectF): RectF {
        val w: Float = inputRect.width()
        val h: Float = inputRect.height()
        var top: Float = inputRect.top
        var bottom: Float = inputRect.bottom
        var left: Float = inputRect.left
        var right: Float = inputRect.right
        if (w > h) {
            top -= (w - h) / 2
            bottom += (w - h) / 2
        } else if (h > w) {
            left -= (h - w) / 2
            right += (h - w) / 2
        }
        return RectF(left, top, right, bottom)
    }

    fun getScaleRect(rect: RectF, imageSize: Size): RectF {
        val expandRatio = 60.0f
        var zoomValue_W = expandRatio * rect.width() / min(imageSize.height, imageSize.width)
        var zoomValue_H = expandRatio * rect.height() / min(imageSize.height, imageSize.width)
        if (rect.top - zoomValue_H < 0 && rect.top >= 0) {
            zoomValue_H = rect.top
        }
        if (rect.left - zoomValue_W < 0 && rect.left >= 0) {
            zoomValue_W = rect.left
        }
        if (rect.right + zoomValue_W > imageSize.width && imageSize.width - rect.right >= 0 && zoomValue_W > imageSize.width - rect.right) {
            zoomValue_W = imageSize.width - rect.right
        }
        if (rect.bottom + zoomValue_H > imageSize.height && imageSize.height - rect.bottom >= 0 && zoomValue_H > imageSize.height - rect.bottom) {
            zoomValue_H = imageSize.height - rect.bottom
        }
        return RectF(
            rect.left - zoomValue_W,
            rect.top - zoomValue_H,
            rect.right + zoomValue_W,
            rect.bottom + zoomValue_H,
        )
    }

    fun normalizeRect(rect: RectF, imageSize: Size): RectF {
        return RectF(
            max(0.0f, rect.left),
            max(0.0f, rect.top),
            min(imageSize.width - 1.0f, rect.right),
            min(imageSize.height - 1.0f, rect.bottom),
        )
    }

    fun preprocessBox(rect: RectF, imageSize: Size): RectF {
        val squareRect = getSquareRect(rect)
        val scaledRect = getScaleRect(squareRect, imageSize)
        val normRect = normalizeRect(scaledRect, imageSize)
        return normRect
    }

    fun boxDetection(inputBitmap: Bitmap): List<DetectionResult> {
        return boxDetector.detect(inputBitmap)
    }

    fun detect(inputBitmap: Bitmap): List<List<PointF>> {
        val detection = boxDetector.detect(inputBitmap)
        return keypointDetection(inputBitmap, detection)
    }

    fun keypointDetection(inputBitmap: Bitmap, detectionResults: List<DetectionResult>): List<List<PointF>> {
        val outputs = mutableListOf<List<PointF>>()
        val imageSize = Size(inputBitmap.width, inputBitmap.height)
        for (result in detectionResults) {
            val normRect = preprocessBox(result.rect, imageSize)
            val croppedImage = Bitmap.createBitmap(
                inputBitmap,
                normRect.left.toInt(),
                normRect.top.toInt(),
                normRect.width().toInt(),
                normRect.height().toInt()
            )

            val keypoints = keypointsDetector.detect(croppedImage)

            for (i in keypoints.indices) {
                keypoints[i].x = normRect.left + keypoints[i].x
                keypoints[i].y = normRect.top + keypoints[i].y
            }

            outputs.add(keypoints)
        }


        return outputs
    }

    fun release() {
        boxDetector.release()
        keypointsDetector.release()
    }

}