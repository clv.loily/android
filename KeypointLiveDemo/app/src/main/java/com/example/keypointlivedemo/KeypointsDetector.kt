package com.example.keypointlivedemo

import android.content.Context
import android.graphics.Bitmap
import android.graphics.PointF
import android.util.Log
import org.tensorflow.lite.DataType
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.CompatibilityList
import org.tensorflow.lite.gpu.GpuDelegate
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import java.io.FileInputStream
import java.io.IOException
import java.nio.FloatBuffer
import java.nio.channels.FileChannel
import java.util.*


class KeypointsDetector(private val context: Context, private val enableGPU: Boolean) {

    companion object {
        var INPUT_IMAGE_WIDTH: Int = 0
        var INPUT_IMAGE_HEIGHT: Int = 0
        private const val MODEL_PATH = "keypoints.tflite"
        private const val NUM_THREADS = 4
        private const val IMAGE_MEAN = 0.0f
        private const val IMAGE_STD = 1.0f
        private const val INPUT_TENSOR_INDEX = 0
        private const val NUM_KEYPOINTS = 5
    }

    private lateinit var model: Interpreter
    private lateinit var imageProcessor: ImageProcessor

    init {
        loadModel()
    }

    private fun loadModel() {
        try {
            val fileDescriptor = context.assets.openFd(MODEL_PATH)
            val fileChannel = FileInputStream(fileDescriptor.fileDescriptor).channel
            val modelFile = fileChannel.map(
                FileChannel.MapMode.READ_ONLY,
                fileDescriptor.startOffset,
                fileDescriptor.declaredLength
            )

            val compatList = CompatibilityList()

            val options = Interpreter.Options().apply{
                if (enableGPU && compatList.isDelegateSupportedOnThisDevice) {
                    // if the device has a supported GPU, add the GPU delegate
                    val delegateOptions = compatList.bestOptionsForThisDevice
                    this.addDelegate(GpuDelegate(delegateOptions))
                } else if (!enableGPU) {
                    // if the GPU is not supported, run on 4 threads
                    this.setNumThreads(NUM_THREADS)
                } else {
                    throw RuntimeException("GPU device is not supported")
                }
            }

            model = Interpreter(modelFile, options)
            val inputShape =
                model.getInputTensor(INPUT_TENSOR_INDEX).shape() // {1, height, width, 3}
            INPUT_IMAGE_HEIGHT = inputShape[1]
            INPUT_IMAGE_WIDTH = inputShape[2]
            Log.d("TFLiteBgrm", "Input shape: ${Arrays.toString(model.getInputTensor(0).shape())}")
            Log.d(
                "TFLiteBgrm",
                "Output shape: ${Arrays.toString(model.getOutputTensor(0).shape())}"
            )
            Log.d("TFLiteBgrm", "Load model success")
        } catch (e: IOException) {
            Log.e("TFLiteBgrm", "Unable to load model")
        }
        imageProcessor = ImageProcessor.Builder()
            .add(
                ResizeOp(INPUT_IMAGE_HEIGHT, INPUT_IMAGE_WIDTH, ResizeOp.ResizeMethod.BILINEAR)
            )
            .add(NormalizeOp(IMAGE_MEAN, IMAGE_STD))
            .build()
    }

    fun detect(inputBitmap: Bitmap): List<PointF> {
        val imageDataType: DataType = model.getInputTensor(INPUT_TENSOR_INDEX).dataType()
        val inputTensorImage = TensorImage(imageDataType)
        inputTensorImage.load(inputBitmap)
        val processedImage = imageProcessor.process(inputTensorImage)
        val outputBuffer = FloatBuffer.allocate(2 * NUM_KEYPOINTS)
        model.run(processedImage.buffer, outputBuffer)
        outputBuffer.rewind()

        val outArray = outputBuffer.array()

        val outKeypoints = mutableListOf<PointF>()
        for (i in 0 until NUM_KEYPOINTS) {
            val x = outArray[i * 2 + 0] * inputBitmap.width
            val y = outArray[i * 2 + 1] * inputBitmap.height
            val point = PointF(x, y)
            outKeypoints.add(point)
        }
        return outKeypoints
    }

    fun release() {
        model.close()
    }

}