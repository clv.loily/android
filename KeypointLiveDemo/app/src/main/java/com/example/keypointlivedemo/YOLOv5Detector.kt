package com.example.keypointlivedemo

import android.content.Context
import android.graphics.Bitmap
import android.graphics.PointF
import android.graphics.RectF
import android.util.Log
import android.util.Size
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.CompatibilityList
import org.tensorflow.lite.gpu.GpuDelegate
import org.tensorflow.lite.support.common.ops.NormalizeOp
import org.tensorflow.lite.support.image.ImageProcessor
import org.tensorflow.lite.support.image.TensorImage
import org.tensorflow.lite.support.image.ops.ResizeOp
import org.tensorflow.lite.support.image.ops.ResizeWithCropOrPadOp
import java.io.FileInputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.FileChannel
import java.util.*
import kotlin.math.max


data class DetectionResult(
    var rect: RectF, val boxConfident: Float,
    val classId: Int, val classConfident: Float
)

fun boxIoU(a: RectF, b: RectF): Float {
    return boxIntersection(a, b) / boxUnion(a, b)
}

fun overlap(x1: Float, w1: Float, x2: Float, w2: Float): Float {
    val l1 = x1 - w1 / 2
    val l2 = x2 - w2 / 2
    val left = if (l1 > l2) l1 else l2
    val r1 = x1 + w1 / 2
    val r2 = x2 + w2 / 2
    val right = if (r1 < r2) r1 else r2
    return right - left
}

fun boxIntersection(a: RectF, b: RectF): Float {
    val w = overlap(
        (a.left + a.right) / 2, (a.right - a.left),
        (b.left + b.right) / 2, (b.right - b.left)
    )
    val h = overlap(
        (a.top + a.bottom) / 2, (a.bottom - a.top),
        (b.top + b.bottom) / 2, (b.bottom - b.top)
    )
    if (w < 0 || h < 0) return 0.0f
    return w * h
}

fun boxUnion(a: RectF, b: RectF): Float {
    val i = boxIntersection(a, b)
    return (a.right - a.left) * (a.bottom - a.top) + (b.right - b.left) * (b.bottom - b.top) - i
}

class Yolov5Detector(private val context: Context, private val enableGPU: Boolean) {

    companion object {
        var INPUT_IMAGE_WIDTH: Int = 0
        var INPUT_IMAGE_HEIGHT: Int = 0
        private const val MODEL_PATH = "yolov5n-224-fp16.tflite"
        private const val NUM_THREADS = 4
        private const val IMAGE_MEAN = 0.0f
        private const val IMAGE_STD = 255.0f
        private const val NMS_THRESHOLD = 0.45f
        private const val BOX_THRESHOLD = 0.4f
        private var NUM_CLASSES = 0
        private const val INPUT_TENSOR_INDEX = 0
        private const val OUTPUT_TENSOR_INDEX = 0
//        private const val NUM_OUT_BOX = 6375
        private const val NUM_OUT_BOX = 3087
    }

    private lateinit var model: Interpreter

    init {
        loadModel()
    }

    private fun loadModel() {
        try {
            val fileDescriptor = context.assets.openFd(MODEL_PATH)
            val fileChannel = FileInputStream(fileDescriptor.fileDescriptor).channel
            val modelFile = fileChannel.map(
                FileChannel.MapMode.READ_ONLY,
                fileDescriptor.startOffset,
                fileDescriptor.declaredLength
            )

            val compatList = CompatibilityList()
            val options = Interpreter.Options().apply {
                if (enableGPU && compatList.isDelegateSupportedOnThisDevice) {
                    // if the device has a supported GPU, add the GPU delegate
                    val delegateOptions = compatList.bestOptionsForThisDevice
                    this.addDelegate(GpuDelegate(delegateOptions))
                } else if (!enableGPU) {
                    // if the GPU is not supported, run on 4 threads
                    this.setNumThreads(NUM_THREADS)
                } else {
                    throw RuntimeException("GPU device is not supported")
                }
            }

            model = Interpreter(modelFile, options)
            val inputShape =
                model.getInputTensor(INPUT_TENSOR_INDEX).shape() // {1, height, width, 3}
            val outputShape = model.getOutputTensor(OUTPUT_TENSOR_INDEX)
                .shape() // {1, NUM_OUT_BOX, (NUM_CLASSES=1 + 5)}
            INPUT_IMAGE_HEIGHT = inputShape[1]
            INPUT_IMAGE_WIDTH = inputShape[2]
            NUM_CLASSES = outputShape[2] - 5
            Log.d("DEMO", "Input shape: ${Arrays.toString(model.getInputTensor(0).shape())}")
            Log.d(
                "DEMO",
                "Output shape: ${Arrays.toString(model.getOutputTensor(0).shape())}"
            )
            Log.d("DEMO", "Load model success")
        } catch (e: IOException) {
            Log.e("DEMO", "Unable to load model")
        }
    }

    fun detect(inputBitmap: Bitmap): List<DetectionResult> {
        val tensorImage = TensorImage.fromBitmap(inputBitmap)
        val cropSize = max(inputBitmap.height, inputBitmap.width)
        val cropOrPadOp = ResizeWithCropOrPadOp(cropSize, cropSize)
        val imageProcessor = ImageProcessor.Builder()
            .add(cropOrPadOp)
            .add(
                ResizeOp(
                    INPUT_IMAGE_WIDTH,
                    INPUT_IMAGE_HEIGHT,
                    ResizeOp.ResizeMethod.BILINEAR
                )
            )
            .add(NormalizeOp(IMAGE_MEAN, IMAGE_STD))
            .build()

        val processedTensorImage = imageProcessor.process(tensorImage)
        val outputBuffer =
            ByteBuffer.allocateDirect(NUM_OUT_BOX * (NUM_CLASSES + 5) * Float.SIZE_BYTES)
        outputBuffer.order(ByteOrder.nativeOrder())
        model.run(processedTensorImage.buffer, outputBuffer)

        outputBuffer.rewind()

        val outFloatArray = FloatArray(NUM_OUT_BOX * (NUM_CLASSES + 5))
        outputBuffer.asFloatBuffer().get(outFloatArray)

        val boxes = mutableListOf<DetectionResult>()
        for (index in 0 until NUM_OUT_BOX) {
            val boxConfidence = outFloatArray[index * (NUM_CLASSES + 5) + 4]
            val classConfidence = outFloatArray[index * (NUM_CLASSES + 5) + 5]
            val confidence = classConfidence * boxConfidence
            if (confidence > BOX_THRESHOLD) {
                val w = outFloatArray[index * (NUM_CLASSES + 5) + 2] * INPUT_IMAGE_WIDTH
                val h = outFloatArray[index * (NUM_CLASSES + 5) + 3] * INPUT_IMAGE_HEIGHT
                val x = outFloatArray[index * (NUM_CLASSES + 5) + 0] * INPUT_IMAGE_WIDTH - w / 2
                val y = outFloatArray[index * (NUM_CLASSES + 5) + 1] * INPUT_IMAGE_HEIGHT - h / 2

                val result = DetectionResult(
                    RectF(x, y, x + w, y + h),
                    boxConfidence, 0, classConfidence
                )
                boxes.add(result)
            }
        }

        val finalBoxes = nms(boxes)
        finalBoxes.forEach {
            it.rect = rescaleBox(
                it.rect,
                Size(INPUT_IMAGE_WIDTH, INPUT_IMAGE_HEIGHT),
                Size(cropSize, cropSize)
            ).apply {
                val tl = cropOrPadOp.inverseTransform(PointF(this.left, this.top), inputBitmap.height, inputBitmap.width)
                val br = cropOrPadOp.inverseTransform(PointF(this.right, this.bottom), inputBitmap.height, inputBitmap.width)
                this.set(tl.x, tl.y, br.x, br.y)
            }
        }

        return finalBoxes
    }

    fun release() {
        model.close()
    }

    private fun rescaleBox(box: RectF, oldImageSize: Size, newImageSize: Size): RectF {
        val xFactor = newImageSize.width.toFloat() / oldImageSize.width
        val yFactor = newImageSize.height.toFloat() / oldImageSize.height

        val newX = box.left * xFactor
        val newY = box.top * yFactor
        val newWidth = box.width() * xFactor
        val newHeight = box.height() * yFactor

        return RectF(newX, newY, newX + newWidth, newY + newHeight)
    }

    private fun nms(boxes: List<DetectionResult>): List<DetectionResult> {
        val nmsList = mutableListOf<DetectionResult>()

        for (classId in 0..NUM_CLASSES) {
            //1.find max confidence per class
            val pq: PriorityQueue<DetectionResult> = PriorityQueue(
                50,
                { lhs, rhs -> // Intentionally reversed to put high confidence at the head of the queue.
                    rhs.boxConfident.compareTo(lhs.boxConfident)
                })

            for (i in boxes.indices) {
                if (boxes[i].classId == classId) {
                    pq.add(boxes[i])
                }
            }

            //2.do non maximum suppression
            while (pq.size > 0) {
                //insert detection with max confidence
                val a = arrayOfNulls<DetectionResult>(pq.size)
                val detections: Array<DetectionResult> = pq.toArray(a)
                val max: DetectionResult = detections[0]
                nmsList.add(max)
                pq.clear()
                for (j in 1 until detections.size) {
                    val detection = detections[j]
                    if (boxIoU(max.rect, detection.rect) < NMS_THRESHOLD) {
                        pq.add(detection)
                    }
                }
            }
        }
        return nmsList
    }

}